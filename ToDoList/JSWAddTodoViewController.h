//
//  JSWAddTodoViewController.h
//  ToDoList
//
//  Created by Mac Attack on 5/21/14.
//  Copyright (c) 2014 Jeff Wenzbauer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JSWTodoItem.h"

@protocol AddTodoDelegate <NSObject>
- (void)didAddTodoItem:(JSWTodoItem *)title;
@end

@interface JSWAddTodoViewController : UIViewController {
    id <AddTodoDelegate>delegate;
    IBOutlet UITextField *taskName;
}

@property (retain) id delegate;
@property (nonatomic, retain) IBOutlet UITextField *taskName;

@end
