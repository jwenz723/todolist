//
//  JSWTodoCell.m
//  ToDoList
//
//  Created by Mac Attack on 5/28/14.
//  Copyright (c) 2014 Jeff Wenzbauer. All rights reserved.
//

#import "JSWTodoCell.h"

@implementation JSWTodoCell
@synthesize delegate, taskName, checkButton;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization Code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setChecked:(BOOL)checked {
    if (checked) {
        [checkButton setImage:[UIImage imageNamed:@"CheckMarkCheck.png"] forState:UIControlStateNormal];
    }
    else {
        [checkButton setImage:[UIImage imageNamed:@"CheckMarkBlank.png"] forState:UIControlStateNormal];
    }
}

- (IBAction)toggleCheck:(id)sender {
    if ([[self delegate] respondsToSelector:@selector(didCheckTask:)]) {
        [[self delegate] didCheckTask:checkButton.tag];
        
    }
}

@end
