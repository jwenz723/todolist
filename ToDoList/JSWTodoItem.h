//
//  JSWTodoItem.h
//  ToDoList
//
//  Created by Mac Attack on 5/28/14.
//  Copyright (c) 2014 Jeff Wenzbauer. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JSWTodoItem : NSObject {
    NSInteger itemId;
    NSString * taskName;
    BOOL complete;
}

@property (readwrite) NSInteger itemId;
@property (nonatomic, retain) NSString *taskName;
@property (readwrite) BOOL complete;

-(id)initWithItemId:(NSInteger)item task:(NSString *)task done:(BOOL)done;

@end
