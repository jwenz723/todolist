//
//  JSWTodoItem.m
//  ToDoList
//
//  Created by Mac Attack on 5/28/14.
//  Copyright (c) 2014 Jeff Wenzbauer. All rights reserved.
//

#import "JSWTodoItem.h"

@implementation JSWTodoItem
@synthesize itemId, taskName, complete;

- (id)initWithItemId:(NSInteger)item task:(NSString *)task done:(BOOL)done{
    if (self = [super init]) {
        self.itemId = item;
        self.taskName = task;
        self.complete = done;
    }
    
    return self;
}

@end
