//
//  JSWAddTodoViewController.m
//  ToDoList
//
//  Created by Mac Attack on 5/21/14.
//  Copyright (c) 2014 Jeff Wenzbauer. All rights reserved.
//

#import "JSWAddTodoViewController.h"

@interface JSWAddTodoViewController ()

@end

@implementation JSWAddTodoViewController
@synthesize delegate, taskName;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIBarButtonItem *saveButton = [[UIBarButtonItem alloc]initWithTitle:@"Save" style:UIBarButtonItemStyleDone target:self action:@selector(addTodoItem:)];
    self.navigationItem.rightBarButtonItem = saveButton;
    self.title = @"New Item";
    
    // Put the cursor into the textbox on the screen
    [taskName becomeFirstResponder];
    
    // Do any additional setup after loading the view from its nib.
}

- (void)addTodoItem:(id)sender {
    if ([[self delegate]respondsToSelector:@selector(didAddTodoItem:)]) {
        [[self delegate] didAddTodoItem:[[JSWTodoItem alloc] initWithItemId:0 task:taskName.text done:NO]];
    }
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
