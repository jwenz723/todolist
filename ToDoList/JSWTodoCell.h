//
//  JSWTodoCell.h
//  ToDoList
//
//  Created by Mac Attack on 5/28/14.
//  Copyright (c) 2014 Jeff Wenzbauer. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TodoCheckDelegate <NSObject>

- (void)didCheckTask:(NSInteger)item;

@end

@interface JSWTodoCell : UITableViewCell {
    id <TodoCheckDelegate> delegate;
    IBOutlet UILabel *taskName;
    IBOutlet UIButton *checkButton;
}

@property (retain) id delegate;
@property (nonatomic, retain) IBOutlet UILabel *taskName;
@property (nonatomic, retain) IBOutlet UIButton *checkButton;

- (void)setChecked:(BOOL)checked;
- (IBAction)toggleCheck:(id)sender;

@end
