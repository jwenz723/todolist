//
//  JSWViewController.h
//  ToDoList
//
//  Created by Mac Attack on 5/21/14.
//  Copyright (c) 2014 Jeff Wenzbauer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JSWAddTodoViewController.h"
#import "JSWTodoCell.h"
#import "JSWTodoItem.h"

@interface JSWViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, AddTodoDelegate, TodoCheckDelegate> {
    NSMutableArray *todoList;
    IBOutlet UITableView *_listTableView;
}

@property (nonatomic, weak) IBOutlet UITableView *listTableView;

- (IBAction)addTodo:(id)sender;

@end
