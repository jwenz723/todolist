//
//  JSWViewController.m
//  ToDoList
//
//  Created by Mac Attack on 5/21/14.
//  Copyright (c) 2014 Jeff Wenzbauer. All rights reserved.
//

#import "JSWViewController.h"
#import "JSWTodoItem.h"
#import "JSWTodoCell.h"
#import "FMDatabase.h"

@interface JSWViewController ()

@end

@implementation JSWViewController
@synthesize listTableView;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    todoList = [[NSMutableArray alloc] init];
    [self updateList];
}

// Get the path where we will have our database
- (NSString *)dataFilePath {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docDirectory = [paths objectAtIndex:0];
    return docDirectory;
}

- (void)updateList {
    // remove all objects before we re-add everything
    [todoList removeAllObjects];
    
    FMDatabase *database = [FMDatabase databaseWithPath:[[self dataFilePath]stringByAppendingPathComponent:@"todo.sqlite"]];
    
    // This will create the database even if it doesn't already exist
    [database open];
    
    // create table
    [database executeUpdate:@"CREATE TABLE IF NOT EXISTS todo (id INTEGER PRIMARY KEY, name VARCHAR(50), complete INTEGER)"];
    
    FMResultSet *results = [database executeQuery:@"SELECT * FROM todo"];

    // loop the the results
    while ([results next]) {
        JSWTodoItem *tmp = [[JSWTodoItem alloc]initWithItemId:[results intForColumn:@"id"] task:[results stringForColumn:@"name"] done:[results boolForColumn:@"complete"]];

        // Add the item into our list
        [todoList addObject:tmp];
    }
    [database close];
    
    // Update the view
    [listTableView reloadData];
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [todoList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *tableCellID = @"TableCellID";
    //UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:tableCellID];
    JSWTodoCell *cell = (JSWTodoCell *)[tableView dequeueReusableCellWithIdentifier:tableCellID];
    
    if (cell == nil) {
//        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:tableCellID];
        
        // When you call loadNibNamed it pulls in all the objects in the Xib file that we created as an array
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"JSWTodoCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
        [cell setDelegate:self];
    }
    
    JSWTodoItem* tmp = (JSWTodoItem *)[todoList objectAtIndex:indexPath.row];
    
    cell.checkButton.tag = indexPath.row;
    [cell setChecked:tmp.complete];
    cell.taskName.text = tmp.taskName;
    
//    cell.textLabel.text = tmp.taskName;
//    //cell.detailTextLabel.text = @"Table Views!";
//    
//    if (tmp.complete) {
//        cell.accessoryType = UITableViewCellAccessoryCheckmark;
//    }
//    else {
//        cell.accessoryType = UITableViewCellAccessoryNone;
//    }
    
    return cell;
}

- (void)didCheckTask:(NSInteger)item {
    FMDatabase *database = [FMDatabase databaseWithPath:[[self dataFilePath]stringByAppendingPathComponent:@"todo.sqlite"]];
    [database open];
    
    [database executeUpdate:@"UPDATE todo SET complete = (?) WHERE id = (?)", [NSNumber numberWithBool:![(JSWTodoItem *)[todoList objectAtIndex:item] complete]],[NSNumber numberWithInt:[(JSWTodoItem *)[todoList objectAtIndex:item] itemId]]];
    
    [database close];
    [self updateList];
}

// Deselect the tableviewcell after it is touched
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    FMDatabase *database = [FMDatabase databaseWithPath:[[self dataFilePath]stringByAppendingPathComponent:@"todo.sqlite"]];
    [database open];
    
    [database executeUpdate:@"UPDATE todo SET complete = (?) WHERE id = (?)", [NSNumber numberWithBool:![(JSWTodoItem *)[todoList objectAtIndex:indexPath.row] complete]],[NSNumber numberWithInt:[(JSWTodoItem *)[todoList objectAtIndex:indexPath.row] itemId]]];
    
    [database close];
    [self updateList];
    
//    JSWTodoItem *tmp = (JSWTodoItem *)[todoList objectAtIndex:indexPath.row];
//    tmp.complete = !tmp.complete;
//    [tableView reloadData];
}

- (void) tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    FMDatabase *database = [FMDatabase databaseWithPath:[[self dataFilePath]stringByAppendingPathComponent:@"todo.sqlite"]];
    [database open];
    
    // delete the object from the database
    [database executeUpdate:@"DELETE FROM todo WHERE id = (?)", [NSNumber numberWithInt:[[todoList objectAtIndex:indexPath.row] itemId]]];
    [database close];
    
    [self updateList];
}

// launch the other view where you type in the item you wish to add
- (IBAction)addTodo:(id)sender {
    JSWAddTodoViewController *vc = [[JSWAddTodoViewController alloc] initWithNibName:@"JSWAddTodoViewController" bundle:nil];
    [vc setDelegate:self];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)didAddTodoItem:(JSWTodoItem *)title {
    FMDatabase *database = [FMDatabase databaseWithPath:[[self dataFilePath]stringByAppendingPathComponent:@"todo.sqlite"]];
    
    // This will create the database even if it doesn't already exist
    [database open];
    
    // insert the new todoItem into the database
    [database executeUpdate:@"INSERT INTO todo (name, complete) VALUES (?, ?)", title.taskName, 0];
    [database close];
    
    // update the view
    [self updateList];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
